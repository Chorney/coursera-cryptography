import byte_methods as bm 
from Crypto.Cipher import AES
import pdb

DEBUG = True

#cbc encrypts the message, with an input key, and iv, key,message, and iv should be input in HEx
def cbcEncrypt(msg,key,iv):
  
  block_array = bm.partitionByteString(bm.createByteString(msg,mode="ASCII"))
  bm.padByteBlockArray(block_array)
 
  key_array = bm.createByteString(key)
  iv_array = bm.createByteString(iv) 

  e_obj = AES.new(bm.convertIntByteArrayByteString(key_array),AES.MODE_ECB)

  if DEBUG == True:
    print(block_array)
    print(key_array)
    print(iv_array)

  xor_elem = iv_array[:]
  cipher_block_array = []
  for block in block_array:
   
    aes_input = bm.convertIntByteArrayByteString(bm.xorByteArray(xor_elem,block))
    
    if DEBUG== True:
      print(aes_input)

    cipher = e_obj.encrypt(aes_input)
    cipher_int = bm.convertByteArrayToIntArray(cipher)
    print(cipher_int)
    cipher_block_array.append(cipher_int) 
    xor_elem = cipher_int[:]

  print(cipher_block_array)

  return iv+bm.convertBlockArrayHexString(cipher_block_array)  

#decrypts a message using cbc, both the msg and key should be in string hex
def cbcDecrypt(cipher_text,key):
  #iv = cipher_text[0:32] -- don't need this as it's already at the beginning''
  key_array = bm.createByteString(key)
  block_array = bm.partitionByteString(bm.createByteString(cipher_text))
  e_obj = AES.new(bm.convertIntByteArrayByteString(key_array),AES.MODE_ECB)

  block_array.reverse()
  decrypt_msg_block_array = []
  for s in xrange(len(block_array)-1):
    msg_block = e_obj.decrypt(bm.convertIntByteArrayByteString(block_array[s]))
    msb_block_int = bm.convertByteArrayToIntArray(msg_block)
    msg_block_int = bm.xorByteArray(msb_block_int,block_array[s+1])
    decrypt_msg_block_array.append(msg_block_int)

  decrypt_msg_block_array.reverse()

  num_pad_bytes = decrypt_msg_block_array[-1][-1]
  
  return bm.convertBlockArrayASCIIString(decrypt_msg_block_array)[0:-1*num_pad_bytes]

    
