import unittest
import cbc_encryption as c_e
import pdb
import byte_methods as bm 
import random

class TestByteMethods(unittest.TestCase):

    def test_cbcEncrypt(self):
      key = "140b41b22a29beb4061bda66b6747e14" 
      cipher_text = c_e.cbcEncrypt("this is a test encryption of a sentence",key,"2bc452cd82f3b8111111111111111111")
      print(cipher_text)

    def test_cbcDecrypt(self):
      key = "140b41b22a29beb4061bda66b6747e14"
      msg_text = c_e.cbcDecrypt("2bc452cd82f3b8111111111111111111816762804aefff3bec07fe62cd8f34652ec4d334129350b3a780f458822672a354b69acb5302f9ce517e58b4443ef3e2",key)
      print(msg_text)

    def test_encryption_decryption(self):

      key = bm.convertIntByteArrayHexString([random.randint(0,255) for blah in xrange(0,16)])
      print(key)

      input_text = "this is another test sentence we need to test and make it extra long"
      cipher_text = c_e.cbcEncrypt(input_text,key,"2bc452cd82f3b8111111111111111111")
      msg_text = c_e.cbcDecrypt(cipher_text,key)
      self.assertEqual(input_text,msg_text)



if __name__ == '__main__':
    unittest.main()
