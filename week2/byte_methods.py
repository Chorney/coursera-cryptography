import re
import pdb

DEBUG = False
BLOCKSIZE = 16 # blocksize in bytes

#createByteString takes an imput string, depending on mode returns an integer array of bytes
def createByteString(input_data,mode="HEX"):
  
  if mode=="HEX":
    bytestring = re.findall('.{1,2}',input_data)
    int_byte_array = [int(byte,16) for byte in bytestring]
  elif mode=="ASCII":
    bytestring = re.findall('.{1}',input_data)
    int_byte_array = [ord(byte) for byte in bytestring]

  return int_byte_array
    
#partitionByteString breaks takes an integer byte array is an input and splits into 16 byte blocks
def partitionByteString(int_byte_array):
  length = 0
  block_byte_array = []
  for byte in int_byte_array:
    
    if length % BLOCKSIZE == 0:
      block_byte_array.append([]) 

    block_byte_array[-1].append(byte)

    length += 1
    
  return block_byte_array 

# xorByte takes an integery byte as input, and the xor with other byte
def xorByte(int_byte1,int_byte2):
   return int_byte1 ^ int_byte2  

def xorByteArray(int_byte_array1,int_byte_array2):
   return [xorByte(byte1,byte2) for byte1,byte2 in zip(int_byte_array1,int_byte_array2)]

def xorByteBlockArray(block_byte_array1,block_byte_array2):
  return [xor_block_array(block1,block2) for block1,block2 in zip(block_byte_array1,block_byte_array2)]

#padByteString pads the bytestring to ensure 16 byte blocks
def padByteBlockArray(block_array):
  
  lba = len(block_array[-1])
  
  if lba < BLOCKSIZE:
    int_padbyte = BLOCKSIZE-(lba % BLOCKSIZE) 
    for i in xrange(0,int_padbyte):
      block_array[-1].append(int_padbyte)
  elif lba == BLOCKSIZE:
    int_padbyte = BLOCKSIZE
    block_array.append([int_padbyte]*int_padbyte)

  return block_array

def convertIntByteArrayHexString(int_array):
  return "".join([ toHexStrFromInt(elem) for elem in int_array])

def convertIntByteArrayASCIIString(int_array):
  return "".join([ toASCIISTRFromInt(elem) for elem in int_array])

def convertBlockArrayHexString(block_array):
  output_str = ""
  for elem in block_array:
    output_str += convertIntByteArrayHexString(elem)
  return output_str

def convertBlockArrayASCIIString(block_array):
  output_str = ""
  for elem in block_array:
    output_str += convertIntByteArrayASCIIString(elem)
  return output_str

def convertIntByteArrayByteString(int_array):
  return str(bytearray.fromhex(convertIntByteArrayHexString(int_array)))

#toHexStrFromInt converts an integer into hex value string
def toHexStrFromInt(i_int):
  if DEBUG== True:
    print(i_int)
  if i_int < 16:
    return '0'+hex(i_int)[2:]
  else:
    return hex(i_int)[2:]

def toASCIISTRFromInt(i_int):
 return chr(i_int)
  
def convertByteArrayToIntArray(byte_string):
  return [ord(elem) for elem in byte_string]  

def increment(byte_array):
  
   byte_id = -1
   byte_array[byte_id] += 1
   byte_array[byte_id] = byte_array[byte_id] % 256

   return incrementByteArray(byte_array,byte_id)

#incrementByteArray a recursion algortihm for incrementing the 16 long byte array
def incrementByteArray(byte_array,byte_id):
  
  if byte_array[byte_id] == 0:
    byte_id -= 1
    if byte_id == -17:
      return [0]*16
    byte_array[byte_id] +=1
    byte_array[byte_id] = byte_array[byte_id] % 256 
    return incrementByteArray(byte_array,byte_id)

  else:
    return byte_array
     
