import byte_methods as bm 
from Crypto.Cipher import AES
import pdb

def ctrEncrypt(msg,key,iv):

  block_array = bm.partitionByteString(bm.createByteString(msg,mode="ASCII"))
  iv_array = bm.createByteString(iv) 

  key_array = bm.createByteString(key)
  e_obj = AES.new(bm.convertIntByteArrayByteString(key_array),AES.MODE_ECB)
  
  print(iv_array)

  cipher_block_array = []
  seed = iv_array[:]
  for block in block_array:
    
    aes_input = bm.convertIntByteArrayByteString(seed)
    

    one_time_pad = e_obj.encrypt(aes_input)
    one_time_pad_int = bm.convertByteArrayToIntArray(one_time_pad)

    cipher_block_array.append(bm.xorByteArray(one_time_pad_int,block)) 
    seed = bm.increment(seed)
    print(seed)
   

  return iv+bm.convertBlockArrayHexString(cipher_block_array)  


def ctrDecrypt(cipher_text,key):

  iv = cipher_text[0:32]
  key_array = bm.createByteString(key)
  block_array = bm.partitionByteString(bm.createByteString(cipher_text[32:]))
  e_obj = AES.new(bm.convertIntByteArrayByteString(key_array),AES.MODE_ECB)

  msg_block_array = []
  seed = bm.createByteString(iv[:])
  for block in block_array:
    
    aes_input = bm.convertIntByteArrayByteString(seed)

    one_time_pad = e_obj.encrypt(aes_input)
    one_time_pad_int = bm.convertByteArrayToIntArray(one_time_pad)

    msg_block_array.append(bm.xorByteArray(one_time_pad_int,block)) 
    seed = bm.increment(seed)
    print(seed)

  return bm.convertBlockArrayASCIIString(msg_block_array)
