import unittest
import byte_methods as bm
import pdb

class TestByteMethods(unittest.TestCase):

    def test_createByteString(self):
      self.assertEqual(bm.createByteString("abe134a8",mode="HEX"),[171, 225, 52, 168])
      self.assertEqual(len(bm.createByteString("this is alot of bytes as it is in ASCII",mode="ASCII")),39)


    def test_partitionByteString(self):
      input_block_array = bm.createByteString("ab1238acfe2821828ead1878eafbbbb123",mode="HEX")
      self.assertEqual(len(bm.partitionByteString(input_block_array)),2)

    def test_toHexStrFromInt(self):
      self.assertEqual(bm.toHexStrFromInt(15),"0f")
      self.assertEqual(bm.toHexStrFromInt(255),"ff")

    def test_convertIntByteArrayHexString(self):
      test_int_byte_array = [12,244,97,15]
      self.assertEqual(bm.convertIntByteArrayHexString(test_int_byte_array),"0cf4610f")
      
    def test_toASCIISTRFromInt(self):
      self.assertEqual(bm.toASCIISTRFromInt(76),"L")

    def test_convertIntByteArrayASCIIString(self):
      self.assertEqual(bm.convertIntByteArrayASCIIString([121,111,117]),"you")

    def test_increment(self):
      byte_array = [10]*16
      copy_array = byte_array[:]
      self.assertEqual(bm.increment(byte_array)[-1],copy_array[-1]+1)

      byte_array = [10]*16
      byte_array[-1] = 255
      copy_array = byte_array[:]
      byte_array = bm.increment(byte_array)
      self.assertEqual(byte_array[-1],0)
      self.assertEqual(byte_array[-2],copy_array[-2]+1)

      byte_array = [10]*16
      byte_array[-1] = 255
      byte_array[-2] = 255
      copy_array = byte_array[:]
      byte_array = bm.increment(byte_array)
      self.assertEqual(byte_array[-1],0)
      self.assertEqual(byte_array[-2],0)
      self.assertEqual(byte_array[-3],copy_array[-3]+1)

      byte_array = [255]*16
      byte_array = bm.increment(byte_array)
      self.assertEqual(byte_array[0],0)
       
    # def test_isupper(self):
    #     self.assertTrue('FOO'.isupper())
    #     self.assertFalse('Foo'.isupper())

    # def test_split(self):
    #     s = 'hello world'
    #     self.assertEqual(s.split(), ['hello', 'world'])
    #     # check that s.split fails when the separator is not a string
    #     with self.assertRaises(TypeError):
    #         s.split(2)

if __name__ == '__main__':
    unittest.main()
