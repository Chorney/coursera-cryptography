import unittest
import ctr_encryption as c_e
import pdb
import byte_methods as bm 
import random

class TestCtrEncryption(unittest.TestCase):

    def test_ctrEncrypt(self):
      key = "140b41b22a29beb4061bda66b6747e14" 
      cipher_text = c_e.ctrEncrypt("this is a test encryption of a sentence",key,"2bc452cd82f3b8111111111111111111")
      print(cipher_text)

    def test_ctrDecrypt(self):
      key = "140b41b22a29beb4061bda66b6747e14"
      msg_text = c_e.ctrDecrypt("2bc452cd82f3b8111111111111111111afb1017371828f8a0f76d212adc93ebb986f4ff0a5161699e7771775d9f02160d347608e4488b0",key)
      print(msg_text)

    def test_encryption_decryption(self):

      key = bm.convertIntByteArrayHexString([random.randint(0,255) for blah in xrange(0,16)])
      print(key)

      input_text = "this is another test sentence we need to test and make it extra long"
      cipher_text = c_e.ctrEncrypt(input_text,key,"2bc452cd82f3b8111111111111111111")
      msg_text = c_e.ctrDecrypt(cipher_text,key)
      self.assertEqual(input_text,msg_text)


if __name__ == '__main__':
    unittest.main()
