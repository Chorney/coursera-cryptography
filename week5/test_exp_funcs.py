import unittest
import pdb
import exp_funcs as ef

class TestByteMethods(unittest.TestCase):

    def test_funcLeftSide(self):
      
      h = 5
      g = 2
      m = 11
      x_1 = 8

      ans = ef.funcLeftSide(h,g,x_1,m)
      self.assertEqual(ans,9)

    def test_funcRightSide(self):
     
      g = 2
      B = 8
      x_0 = 5
      m = 11
      ans = ef.funcRightSide(g,B,x_0,m)
      self.assertEqual(ans,1)


if __name__ == '__main__':
    unittest.main()
