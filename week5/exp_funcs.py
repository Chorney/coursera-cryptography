import pdb
import gmpy2
from gmpy2 import mpz

def funcLeftSide(h,g,x_1,m):
  denominator = gmpy2.powmod(mpz(g),mpz(x_1),mpz(m))
  leftside = gmpy2.divm(mpz(h),denominator,mpz(m))
  return leftside

def funcRightSide(g,B,x_0,m):

  f_p = gmpy2.powmod(mpz(g),mpz(B),mpz(m))
  s_p = gmpy2.powmod(f_p,mpz(x_0),mpz(m))
 
  return s_p
