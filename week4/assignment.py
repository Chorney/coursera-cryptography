import requests
import sys
sys.path.append("/home/starrover/dev/online-courses/coursera/cryptography1/Week2")
import byte_methods as bm
import pdb

ct = "f20bdba6ff29eed7b046d1df9fb7000058b1ffb4210a580f748b4ac714c001bd4a61044426fb515dad3f21f18aa577c0bdf302936266926ff37dbf7035d5eeb4"
int_array = bm.createByteString(ct)
block_array = bm.partitionByteString(int_array)
iv = block_array[0]

guess_array = [0]*16
padding_array = [0]*16

for t in xrange(0,16):

  padding_byte = t+1

  for p in range(0,padding_byte):
    index = (p+1)*-1
    padding_array[index] = padding_byte

  #if padding_byte ==9:
  #  pdb.set_trace()

  for s in xrange(0,256):
    
    #pdb.set_trace()
  
    guess_array[-1*padding_byte] = s
    #NOTE: when manually changing things don't forget to change the block_array index here as well needs to match up with tmp array selecteed block below
    tmp_array = bm.xorByteArray(padding_array,bm.xorByteArray(block_array[0],guess_array))
    #pdb.set_trace()

    #to get the padding oracle to work you need to just send up to the block message you wanna decode, have block{message_needed-1} set as the tmp_array

    block0 = bm.convertIntByteArrayHexString(tmp_array)
    block1 = bm.convertIntByteArrayHexString(block_array[1])
    block2 = bm.convertIntByteArrayHexString(block_array[2])
    block3 = bm.convertIntByteArrayHexString(block_array[3])

    #pdb.set_trace()
    query_string = block0+block1#+block2#+block3

    response = requests.get("http://crypto-class.appspot.com/po?er="+query_string)

    #print("bytenum: "+str(s)+" padding_byte: "+str(padding_byte))
    print(response.status_code)
    if response.status_code == 404:
     break

    #the actual message has padding bytes, when the padding oracle byting matches the actual message padding bytes, then you get a response 200, as they cancel out
    if response.status_code == 200 and t>0:
     break

  print(guess_array)


#NOTE: to get these results I manually altred the block0,block1, ... selected to the padding oracle, 

#first block (as the first one in the cipher text is the IV) = 
b1 = [84, 104, 101, 32, 77, 97, 103, 105, 99, 32, 87, 111, 114, 100, 115, 32] 
#second last block = 
b2 = [97, 114, 101, 32, 83, 113, 117, 101, 97, 109, 105, 115, 104, 32, 79, 115] 
#last block = 
b3 = [115, 105, 102, 114, 97, 103, 101, 9, 9, 9, 9, 9, 9, 9, 9, 9]


#MANUAllY COPY PASTE INTO A BLOCK ARRAY
answer_array = [b1,b2,b3]
#AND THEN IN A PYTHON INERPRETER import byte_methods from week2, and then run:
bm.convertBlockArrayASCIIString(answer_array)
