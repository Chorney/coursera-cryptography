import pdb
import numpy as np
import gmpy2
from gmpy2 import mpz

#factors the composite, smallest prime is the first element of the list
def factor_composite(N):
  A = gmpy2.add(gmpy2.isqrt(N),1)
  x = gmpy2.isqrt(gmpy2.sub(gmpy2.mul(A,A),N))

  prime1 = gmpy2.sub(A,x)
  prime2 = gmpy2.add(A,x)

  l = [prime1,prime2]
  l.sort()

  return l 

  

chal1 = mpz(179769313486231590772930519078902473361797697894230657273430081157732675805505620686985379449212982959585501387537164015710139858647833778606925583497541085196591615128057575940752635007475935288710823649949940771895617054361149474865046711015101563940680527540071584560878577663743040086340742855278549092581)
chal2 = mpz(648455842808071669662824265346772278726343720706976263060439070378797308618081116462714015276061417569195587321840254520655424906719892428844841839353281972988531310511738648965962582821502504990264452100885281673303711142296421027840289307657458645233683357077834689715838646088239640236866252211790085787877)
chal3 = mpz(720062263747350425279564435525583738338084451473999841826653057981916355690188337790423408664187663938485175264994017897083524079135686877441155132015188279331812309091996246361896836573643119174094961348524639707885238799396839230364676670221627018353299443241192173812729276147530748597302192751375739387929)

#challange 1

[p,q] = factor_composite(chal1)

print("Anwer1: \n")
print(p)
print(q)

# challange 2

for i in xrange(0,np.power(2,20)):
  
  A_try = gmpy2.add(gmpy2.isqrt(chal2),i)

  diff = gmpy2.sub(gmpy2.mul(A_try,A_try),chal2)
  
  if diff > 0:
    x = gmpy2.isqrt(diff)

    p1 = gmpy2.sub(A_try,x)
    p2 = gmpy2.add(A_try,x)

    check_mult = gmpy2.mul(p1,p2)

    if check_mult == chal2:
      break


l = [p1,p2]
l.sort()

print("Challange 2 smallest prime: \n")
print(l[0])


#Challange 3
# for this solution we know (3p+2q)/2 is near sqrt(6N)
# but numerator is odd, so multiply top by 2, than we know (6p+4q)/2 is near 2*sqrt(6N)
# well (6p+4q) is even, and half way point is near 2*sqrt(6N)
# so as the same with the notes for challnage 1, then 6p = A-x and 4q = A+x where A = (6p+4q)/2 = 3p+2q ~= 2*sqrt(6N)
# 24*pq = (A-x)*(A+x) = (A-x^2) = 24N
# then x = sqrt(A^2 - 24N) where A = 2*sqrt(N)

counter = 0
while True:

  A_try = 2*gmpy2.isqrt(6*chal3)+counter

  discr = gmpy2.sub(gmpy2.mul(A_try,A_try),24*chal3)

  if discr > 0:

    x = gmpy2.isqrt(discr)

    p = gmpy2.div(gmpy2.sub(A_try,x),6)
    q = gmpy2.div(gmpy2.add(A_try,x),4)

    if gmpy2.mul(p,q) == chal3:
      print("done challange 3: \n")
      l = [p,q]
      l.sort()
      print(l[0])
      break

  counter = counter + 1

# challange 4

p = mpz(13407807929942597099574024998205846127479365820592393377723561443721764030073662768891111614362326998675040546094339320838419523375986027530441562135724301)
q = mpz(13407807929942597099574024998205846127479365820592393377723561443721764030073778560980348930557750569660049234002192590823085163940025485114449475265364281)

phi_n = (p-1)*(q-1)

e = 65537

# d is the exponent such d*e = 1 mod (phi_n)
# one would use the extended euclidean algorithm as gcd(e,phi_n) = 1, find k,m s.t 1 = k*e + m*phi_n
# to solve for assignment used built in function

d = gmpy2.divm(1,e,phi_n)

c = mpz(22096451867410381776306561134883418017410069787892831071731839143676135600120538004282329650473509424343946219751512256465839967942889460764542040581564748988013734864120452325229320176487916666402997509188729971690526083222067771600019329260870009579993724077458967773697817571267229951148662959627934791540)
msg_encoded = gmpy2.powmod(c,d,chal1)

msg_hex = hex(msg_encoded).split("00")[1]

#after decoding the msg_hex to ascii, get 
answer = "Factoring lets us break RSA."
